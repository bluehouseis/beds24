<!-- PROJECT TITLE -->
  <h1 align="center">Booking page (beds24)</h1>

## Application Description

booking page connecting to beds24 API

## Technology Stack

| Technology   | Description                                                              |
| ------------ | ------------------------------------------------------------------------ |
| React.js     | javaScript library                                                       |
| Express  |  back end web application framework                                                          |
| Beds24 API       | booking page API                                                      |

## Features

1. select date

2. book a room


## How to use the app

**Step #1** - Clone the project

```bash
$ git clone https://@bitbucket.org/bluehouseis/beds24.git
```

**Step #2**

```
cd client 
```

- Install dependencies via npm or yarn: _npm i_ OR _yarn_
```
cd .. & cd back 
```

- Install dependencies via npm or yarn: _npm i_ OR _yarn_
```
in back folder
```

- Website & Server: To start it, npm run dev

## How to use the app (method 2)

**Step #1** - Clone the project

```bash
$ git clone https://@bitbucket.org/bluehouseis/beds24.git
```

**Step #2** - Execute code

- First go to client side
```
cd client 
```

- Install dependencies via npm or yarn: 
```
npm i OR yarn i
```
- then go to back (server) folder
```
cd .. & cd back 
```

- Install dependencies via npm or yarn: 
```
npm i OR yarn i
```
- execute back end code...
```
npm run server
```

- Create a new Terminal, Go to client folder and run the client code
```
cd client
npm run start
```

